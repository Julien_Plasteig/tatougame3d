﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    Vector3 position;
	// Update is called once per frame
	void Update () {
        position = new Vector3(transform.position.x, transform.position.y + 10, transform.position.z);
        Camera.main.transform.position = position;
	}
}
