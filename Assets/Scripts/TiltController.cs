﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiltController : MonoBehaviour {

    private Rigidbody rigid;
    [SerializeField]
    private float speed;
    [SerializeField]
    private Vector3 tilt;

    private Quaternion localRotation; // ajustable speed from Inspector in Unity editor

    // Use this for initialization
    void Start()
    {
        // copy the rotation of the object itself into a buffer
        localRotation = transform.rotation;
    }


    void Update() 
    {
        // find speed based on delta
        float curSpeed = Time.deltaTime * speed;
        tilt = Input.acceleration;
        tilt = Quaternion.Euler(90, 0, 0) * tilt;
        // first update the current rotation angles with input from acceleration axis
        localRotation.x = tilt.x * curSpeed;
        localRotation.z = tilt.z * curSpeed;

        // then rotate this object accordingly to the new angle
        transform.rotation = localRotation;

    }

}
