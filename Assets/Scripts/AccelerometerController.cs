﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AccelerometerController : MonoBehaviour {
    private Rigidbody rigid;
    [SerializeField]
    private float speed;
    [SerializeField]
    private Vector3 tilt;

    // Use this for initialization
    void Start () {
        rigid = GetComponent<Rigidbody>();
        speed = 5.0f;
        tilt = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {

        tilt = Input.acceleration;
        tilt = Quaternion.Euler(90, 0, 0) * tilt;
        rigid.AddForce(tilt.x*speed,0,tilt.z * speed);
        Debug.DrawRay(transform.position + Vector3.up, tilt, Color.red);
	}
}
