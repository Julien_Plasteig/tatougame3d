﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Menu : MonoBehaviour {

    private void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
    }

    public void AppExit()
    {
        Application.Quit();
    }

    public void LaunchGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
